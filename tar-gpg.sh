#!/bin/bash
# TAR and ENCRYPT
# (C) 2023 SANOMYA
# v1.0.2
: '
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'
if [ -z $6 ]; then echo "Tar and encrypt with GPG. Usage: $0 -i [to_encrypt] -o [output] -k [gpg_key]"; exit 1; fi

while getopts ":i:o:k:d" opt; do
    case $opt in
        i)
            input=$OPTARG
        ;;
        o)
            destination=$OPTARG
        ;;
        k)
            gpg_key=$OPTARG
        ;;
        d)
            date=true
        ;;
        *)
            echo "Tar and encrypt with GPG. Usage: $0 -i [to_encrypt] -o [output] -k [gpg_key]"
            exit 1
            ;;
    esac
done



#Get today's date and store it as variable.
datum=$(date +%Y-%m-%d);

if [ "$date" = true ]; then destination=$destination-$datum.gpg; fi

if [[ -d $input ]]
then
    totar=$(basename "$input")
	#tar the target directory and encrypt with gpg
	cd $input
	cd ..
	tar -I "pbzip2 -9" -c $totar  | gpg2 --output $destination --encrypt --recipient $gpg_key
else
	#tar the target file if it exists and encrypt with gpg
	if [[ -f $input ]]; then tar -I "pbzip2 -9" -c $1 | gpg --output $destination --encrypt --recipient $3; else echo "File not found"; fi
fi

#change permissions to read-only.
if [[ -f $destination ]]; then chmod 400 $destination; fi
