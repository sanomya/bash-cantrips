#!/bin/bash
# DECRYPT and UNTAR
# (C) 2023 SANOMYA
# v 1.0
: '
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'
if [ -z $3 ]; then echo "Usage: $0 -d [to_decrypt] -o [output]"; exit 1; fi

while getopts ":d:o:" opt; do
    case $opt in
        d)
            target=$OPTARG
        ;;
        o)
            destination=$OPTARG
        ;;
        *)
            echo "Usage: $0 -d [to_decrypt] -o [output]"
            exit 1
            ;;
    esac
done

#tar the target directory or file and encrypt with gpg
gpg -d $target | tar -C $destination -f - -xj 

