# Bash cantrips

## Description
Collection of bash tools I've found to be useful on a daily basis.

## Installation
```
git clone https://gitlab.com/sanomya/bash-cantrips
```

## Usage
tar-gpg.sh will pipe a tar archive using pbzip to gpg for encryption and will optionally append a date to the file name with -d. 

```
sh tar-gpg.sh -i file-to-encrypt -o destination-file -k gpg-public-key
```

decrypt-untar.sh will decrypt gpg and then untar it, it's the reverse of tar-gpg.sh.

```
sh decrypt-untar.sh -d file-to-decrypt -o path-to-untar-to
```

## Authors
Sanomya

## License
GPLv3

